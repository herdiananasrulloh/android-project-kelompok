import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Radea extends StatelessWidget {
  static String tag = 'radea';
  var herdiana = ['assets/radea.jpeg'];

  @override
  Widget build(BuildContext context) {
    final radea = Hero(
      tag: 'radea',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
            radius: 72.0,
            backgroundColor: Colors.transparent,
            backgroundImage: AssetImage('assets/radea.jpeg')),
      ),
    );

    final welcome = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Radea Ahmad Wijaya',
        style: TextStyle(fontSize: 28.0, color: Colors.white),
      ),
    );

    final lorem = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Izin memperkenalkan nama saya Radea Ahmad Wijaya, kuliah di Sekolah Tinggi Teknologi Bandung semester 5 jurusan teknik informatika. Saya lahir di Bandung, 03 Juni 1998.  Hobi saya Olahraga, selain olahraga saya juga suka camping di alam terbuka. Alamat tinggal rumah saya di majalaya kab.Bandung . Terimakasih',
        style: TextStyle(fontSize: 16.0, color: Colors.white),
      ),
    );

    final body = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(28.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Colors.blue,
          Colors.lightBlueAccent,
        ]),
      ),
      child: Column(
        children: <Widget>[radea, welcome, lorem],
      ),
    );

    return Scaffold(
      body: body,
    );
  }
}
