import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Herdiana extends StatelessWidget {
  static String tag = 'herdiana';
  var herdiana = ['assets/herdi.jpg'];

  @override
  Widget build(BuildContext context) {
    final herdiana = Hero(
      tag: 'herdiana',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
            radius: 72.0,
            backgroundColor: Colors.transparent,
            backgroundImage: AssetImage('assets/herdi.jpg')),
      ),
    );

    final welcome = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Herdiana Nasrulloh',
        style: TextStyle(fontSize: 28.0, color: Colors.white),
      ),
    );

    final lorem = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Perkenalkan, saya Herdiana Nasrulloh seorang mahasiswa Sekolah Tinggi Teknologi Bandung jurusan Teknik Informatika yang lahir pada tanggal 11 Juni 2000 di Kota Bandung beralamat rumah di Jl. Sindang Sari No.19 memiliki hobi yaitu olahraga , camping dan bermain game sepak bola. Saya memiliki cita - cita ingin menjadi seorang yang bisa bermanfaat untuk orang banyak',
        style: TextStyle(fontSize: 16.0, color: Colors.white),
      ),
    );

    final body = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(28.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Colors.blue,
          Colors.lightBlueAccent,
        ]),
      ),
      child: Column(
        children: <Widget>[herdiana, welcome, lorem],
      ),
    );

    return Scaffold(
      body: body,
    );
  }
}
