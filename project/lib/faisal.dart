import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Faisal extends StatelessWidget {
  static String tag = 'faisal';
  var herdiana = ['assets/faisal.jpeg'];

  @override
  Widget build(BuildContext context) {
    final faisal = Hero(
      tag: 'faisal',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
            radius: 72.0,
            backgroundColor: Colors.transparent,
            backgroundImage: AssetImage('assets/faisal.jpeg')),
      ),
    );

    final welcome = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Faisal Ghafar Alihsani',
        style: TextStyle(fontSize: 28.0, color: Colors.white),
      ),
    );

    final lorem = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Perkenalkan nama saya Faisal Ghafar Alihsani , saat ini saya sedang mengambil kejuruan atas di sekolah tinggi teknologi Bandung dengan nomor induk mahasiswa saya 18111195, saya lahir di Bandung tanggal 27 Oktober 1996,tempat tinggal saya di kp cihurip rt03  RW 14, itu adalah biodata singkat saya jika membutuhkan info lebih lanjut bisa kontak nohp/wa di 081224187475. Disekolah tinggi teknologi Bandung saya mengampu jurusan teknik informatika. Saya mengambil jurusan informatika dikarenakan dijaman era digital sekarang banyak sekali yang sudah beralih ke digital seperti ojeg online/pembelian online maka dari itu saya memperdalam ilmu saya tentang it di sekolah tinggi teknologi Bandung. Terimakasih!',
        style: TextStyle(fontSize: 16.0, color: Colors.white),
      ),
    );

    final body = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(28.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Colors.blue,
          Colors.lightBlueAccent,
        ]),
      ),
      child: Column(
        children: <Widget>[faisal, welcome, lorem],
      ),
    );

    return Scaffold(
      body: body,
    );
  }
}
