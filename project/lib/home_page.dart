import 'package:flutter/material.dart';
import 'package:project/herdiana.dart';
import 'package:project/derry.dart';
import 'package:project/faisal.dart';
import 'package:project/radea.dart';

// ignore: must_be_immutable
class HomePage extends StatelessWidget {
  static String tag = 'home-page';
  var home = ['assets/logo.jpg'];

  @override
  Widget build(BuildContext context) {
    final home = Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
            radius: 72.0,
            backgroundColor: Colors.transparent,
            backgroundImage: AssetImage('assets/logo.jpg')),
      ),
    );

    final welcome = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Selamat Datang',
        style: TextStyle(fontSize: 28.0, color: Colors.white),
      ),
    );

    final lorem = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Silahkan pilih button nama dibawah agar mengetahui identitasnya masing - masing',
        style: TextStyle(fontSize: 16.0, color: Colors.white),
      ),
    );

    // ignore: non_constant_identifier_names
    final button = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.lightBlueAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            Navigator.of(context).pushNamed(Herdiana.tag);
          },
          color: Colors.lightBlueAccent,
          child:
              Text('Herdiana Nasrulloh', style: TextStyle(color: Colors.white)),
        ),
      ),
    );

    // ignore: non_constant_identifier_names
    final button1 = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.lightBlueAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            Navigator.of(context).pushNamed(Derry.tag);
          },
          color: Colors.lightBlueAccent,
          child: Text('Derry Efrillian Sephina',
              style: TextStyle(color: Colors.white)),
        ),
      ),
    );

    // ignore: non_constant_identifier_names
    final button2 = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.lightBlueAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            Navigator.of(context).pushNamed(Faisal.tag);
          },
          color: Colors.lightBlueAccent,
          child: Text('Faisal Ghafar Alihsani',
              style: TextStyle(color: Colors.white)),
        ),
      ),
    );

    // ignore: non_constant_identifier_names
    final button3 = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.lightBlueAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            Navigator.of(context).pushNamed(Radea.tag);
          },
          color: Colors.lightBlueAccent,
          child:
              Text('Radea Ahmad Wijaya', style: TextStyle(color: Colors.white)),
        ),
      ),
    );

    final body = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(28.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Colors.blue,
          Colors.lightBlueAccent,
        ]),
      ),
      child: Column(
        children: <Widget>[
          home,
          welcome,
          lorem,
          button,
          button1,
          button2,
          button3
        ],
      ),
    );

    return Scaffold(
      body: body,
    );
  }
}
